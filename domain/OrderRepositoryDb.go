package domain

import (
	"database/sql"

	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking-lib/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type OrderRepositoryDb struct {
	client *sqlx.DB
}

func (d OrderRepositoryDb) FindAll(status string) ([]Order, *errs.AppError) {
	var err error
	orders := make([]Order, 0)

	//if status == 0 {
	findAllSql :=
		`select
	COALESCE(order_date, '') as order_date
	,id
	,customer_id
	,COALESCE(ship_city, '') as ship_city
	,COALESCE(ship_country_region, '') as ship_country_region
	,COALESCE(ship_name, 0) as ship_name
	from orders`

	err = d.client.Select(&orders, findAllSql)

	if err != nil {
		logger.Error("Error while querying order table " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	return orders, nil
}

func (d OrderRepositoryDb) ById(customer_id string) (*Order, *errs.AppError) {
	orderSql :=
		`select  
	COALESCE(order_date, '') as order_date
	,id
	,customer_id
	,COALESCE(ship_city, '') as ship_city
	,COALESCE(ship_country_region, '') as ship_country_region
	,COALESCE(ship_name, 0) as ship_name
	from orders
	 where customer_id = ?
	 `

	var o Order
	err := d.client.Get(&o, orderSql, customer_id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("Order history not found")
		} else {
			logger.Error("Error while scanning order history " + err.Error())
			return nil, errs.NewUnexpectedError("Unexpected database error")
		}
	}
	return &o, nil
}

func NewOrderRepositoryDb(dbClient *sqlx.DB) OrderRepositoryDb {
	return OrderRepositoryDb{dbClient}
}
