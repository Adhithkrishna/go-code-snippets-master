package domain

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/dto"
)

type Order struct {
	OrderDate   string `db:"order_date"`
	OrderID     string `db:"id"`
	CustomerID  string `db:"customer_id"`
	ShipCity    string `db:"ship_city"`
	ShipCountry string `db:"ship_country_region"`
	ShipName    string `db:"ship_name"`
}

func (o Order) ToDto() dto.OrderResponse {
	return dto.OrderResponse{
		OrderDate:   o.OrderDate,
		OrderID:     o.OrderID,
		CustomerID:  o.CustomerID,
		ShipCity:    o.ShipCity,
		ShipCountry: o.ShipCountry,
		ShipName:    o.ShipName,
	}
}

type OrderRepository interface {
	FindAll(status string) ([]Order, *errs.AppError)
	ById(string) (*Order, *errs.AppError)
}
