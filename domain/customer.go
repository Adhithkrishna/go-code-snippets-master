package domain

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/dto"
)

type Customer struct {
	Address      string `db:"address"`
	City         string `db:"city"`
	CompanyName  string `db:"company"`
	ContactName  string `db:"first_name"`
	ContactTitle string `db:"job_title"`
	Country      string `db:"country_region"`
	CustomerID   int    `db:"id"`
	Fax          string `db:"fax_number"`
	Phone        string `db:"business_phone"`
	PostalCode   string `db:"zip_postal_code"`
	Region       string `db:"state_province"`
}

func (c Customer) ToDto() dto.CustomerResponse {
	return dto.CustomerResponse{
		Address:      c.Address,
		City:         c.City,
		CompanyName:  c.CompanyName,
		ContactName:  c.ContactName,
		ContactTitle: c.ContactTitle,
		Country:      c.Country,
		CustomerID:   c.CustomerID,
		Fax:          c.Fax,
		Phone:        c.Phone,
		PostalCode:   c.PostalCode,
		Region:       c.Region,
	}
}

type CustomerRepository interface {
	FindAll(status string) ([]Customer, *errs.AppError)
	ById(string) (*Customer, *errs.AppError)
}
