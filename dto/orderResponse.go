package dto

type OrderResponse struct {
	OrderDate   string `json:"orderDate"`
	OrderID     string `json:"orderId"`
	CustomerID  string `json:"customerId"`
	ShipCity    string `json:"shipCity"`
	ShipCountry string `json:"shipCountry"`
	ShipName    string `json:"shipName"`
}
