package dto

type NewCustomerResponse struct {
	CustomerID string `json:"customer_id"`
}
