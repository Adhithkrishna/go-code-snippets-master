package dto

import (
	"github.com/ashishjuyal/banking-lib/errs"
)

type NewCustomerRequest struct {
	Address      string `json:"address"`
	City         string `json:"city"`
	CompanyName  string `json:"company"`
	ContactName  string `json:"contactName"`
	ContactTitle string `json:"jobTitle"`
	Country      string `json:"country"`
	CustomerID   string `json:"customerId"`
	Fax          string `json:"fax"`
	Phone        string `json:"phone"`
	PostalCode   string `json:"postalCode"`
	Region       string `json:"region"`
}

func (r NewCustomerRequest) Validate() *errs.AppError {
	//if r.Region < "5000" {
	//	return errs.NewValidationError("Customer is not created")
	//}
	//if strings.ToLower(r.Region) != "USA" && strings.ToLower(r.Region) != "England" {
	//	return errs.NewValidationError("Customer is in USA")
	//}
	return nil
}
