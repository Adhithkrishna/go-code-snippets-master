package dto

type CustomerResponse struct {
	Address      string `json:"address"`
	City         string `json:"city"`
	CompanyName  string `json:"company"`
	ContactName  string `json:"contactName"`
	ContactTitle string `json:"jobTitle"`
	Country      string `json:"country"`
	CustomerID   int    `json:"customerId"`
	Fax          string `json:"fax"`
	Phone        string `json:"phone"`
	PostalCode   string `json:"postalCode"`
	Region       string `json:"region"`
}
