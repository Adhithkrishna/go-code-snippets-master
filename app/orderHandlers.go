package app

import (
	"encoding/json"
	"net/http"

	"github.com/ashishjuyal/banking/service"
	"github.com/gorilla/mux"
)

type OrderHandlers struct {
	service service.OrderService
}

func (oh *OrderHandlers) getAllOrders(w http.ResponseWriter, r *http.Request) {

	status := r.URL.Query().Get("status")

	orders, err := oh.service.GetAllOrder(status)

	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, orders)
	}
}

func (oh *OrderHandlers) getOrder(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customer_id := vars["customer_id"]

	order, err := oh.service.GetOrder(customer_id)
	if err != nil {
		writeOrderResponse(w, err.Code, err.AsMessage())
	} else {
		writeOrderResponse(w, http.StatusOK, order)
	}
}

func writeOrderResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
