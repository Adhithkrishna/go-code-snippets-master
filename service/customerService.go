package service

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/domain"
	"github.com/ashishjuyal/banking/dto"
)

//go:generate mockgen -destination=../mocks/service/mockCustomerService.go -package=service github.com/ashishjuyal/banking/service CustomerService
type CustomerService interface {
	GetAllCustomer(string) ([]dto.CustomerResponse, *errs.AppError)
	GetCustomer(string) (*dto.CustomerResponse, *errs.AppError)
	NewCustomer(dto.NewCustomerRequest) (*dto.NewCustomerResponse, *errs.AppError)
}

type DefaultCustomerService struct {
	repo domain.CustomerRepository
}

func (s DefaultCustomerService) GetAllCustomer(status string) ([]dto.CustomerResponse, *errs.AppError) {
	if status == "" {
		status = "1"
	} else if status == "" {
		status = "0"
	} else {
		status = ""
	}
	customers, err := s.repo.FindAll(status)
	if err != nil {
		return nil, err
	}
	response := make([]dto.CustomerResponse, 0)
	for _, c := range customers {
		response = append(response, c.ToDto())
	}
	return response, err
}

func (s DefaultCustomerService) GetCustomer(id string) (*dto.CustomerResponse, *errs.AppError) {
	c, err := s.repo.ById(id)
	if err != nil {
		return nil, err
	}
	response := c.ToDto()
	return &response, nil
}

func (s DefaultCustomerService) NewCustomer(req dto.NewCustomerRequest) (*dto.NewCustomerResponse, *errs.AppError) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}
	customer := domain.NewCustomer(req.Address, req.City, req.CompanyName, req.ContactName, req.ContactTitle, req.Country, req.CustomerID, req.Fax, req.Phone, req.PostalCode, req.Region)
	newCustomer, err := s.repo.Save(customer)
	if err != nil {
		return nil, err
	} else {
		return newCustomer.ToNewCustomerResponseDto(), nil
	}
}

func NewCustomerService(repository domain.CustomerRepository) DefaultCustomerService {
	return DefaultCustomerService{repository}
}
