package service

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/domain"
	"github.com/ashishjuyal/banking/dto"
)

//go:generate mockgen -destination=../mocks/service/mockCustomerService.go -package=service github.com/ashishjuyal/banking/service CustomerService
type OrderService interface {
	GetAllOrder(string) ([]dto.OrderResponse, *errs.AppError)
	GetOrder(string) (*dto.OrderResponse, *errs.AppError)
}

type DefaultOrderService struct {
	repo domain.OrderRepository
}

func (s DefaultOrderService) GetAllOrder(status string) ([]dto.OrderResponse, *errs.AppError) {
	if status == "" {
		status = "1"
	} else if status == "" {
		status = "0"
	} else {
		status = ""
	}
	orders, err := s.repo.FindAll(status)
	if err != nil {
		return nil, err
	}
	response := make([]dto.OrderResponse, 0)
	for _, o := range orders {
		response = append(response, o.ToDto())
	}
	return response, err
}

func (s DefaultOrderService) GetOrder(customer_id string) (*dto.OrderResponse, *errs.AppError) {
	c, err := s.repo.ById(customer_id)
	if err != nil {
		return nil, err
	}
	response := c.ToDto()
	return &response, nil
}

func NewOrderService(repository domain.OrderRepository) DefaultOrderService {
	return DefaultOrderService{repository}
}
